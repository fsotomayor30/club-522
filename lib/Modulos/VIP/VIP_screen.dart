import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class VIPPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: ListView(
              children: <Widget>[
                ListTile(
                  title: Container(
                    height: 250,
                    child: Stack(
                      children: <Widget>[
                        Image.asset("assets/Imagenes/cafeBanner.jpg", width: mediaQueryData.width, height: mediaQueryData.height*0.2,),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: Image.asset("assets/Imagenes/usuario.png", width: 150, height: 150)),

                        // Max Size

/*                    Container(
                        color: Colors.blue,
                        height: 300.0,
                        width: 300.0,
                      ),
                      Container(
                        color: Colors.pink,
                        height: 150.0,
                        width: 150.0,
                      )*/
                      ],
                    ),
                  ),

                ),
                Container(height: 20,),

                Card(
                  child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow,),
                        FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow,),
                        FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow,),
                        FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow,),
                        FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow,),
                      ],
                    ),
                    subtitle:Text("Estrellas",textAlign: TextAlign.center, style: TextStyle(
                        fontFamily: "SenBold"
                    ),
                    ),
                  ),
                ),
                Container(height: 20,),
                Card(
                  child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("1.000", style: TextStyle(
                            fontFamily: "SenBold",
                          fontSize: 40
                        ),
                          textAlign: TextAlign.center,)
                      ],
                    ),
                    subtitle:Text("Puntos",textAlign: TextAlign.center, style: TextStyle(
                        fontFamily: "SenBold"
                    ),
                    ),
                  ),
                ),
/*                ListTile(
                    title: Text("Ubicación", style: TextStyle(
                        fontFamily: "SenBold"
                    ),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Isabel Riquelme #522',style: TextStyle(
                            fontFamily: "SenRegular"
                        ),),
                        Text('Chillán',style: TextStyle(
                            fontFamily: "SenRegular"
                        ),),
                      ],),
                    leading: Icon(Icons.location_on),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: () async {
                      if (await canLaunch(
                          "https://www.google.com/maps/place/Club+522/@-36.607222,-72.1016447,17z/data=!3m1!4b1!4m5!3m4!1s0x9668d7d5e60fdf21:0x8b2342473a4580ca!8m2!3d-36.607222!4d-72.099456")) {
                        await launch(
                            "https://www.google.com/maps/place/Club+522/@-36.607222,-72.1016447,17z/data=!3m1!4b1!4m5!3m4!1s0x9668d7d5e60fdf21:0x8b2342473a4580ca!8m2!3d-36.607222!4d-72.099456");
                      }
                    }
                ),
                ListTile(
                  title: Text("Teléfono",style: TextStyle(
                      fontFamily: "SenBold"
                  ),),
                  leading: FaIcon(FontAwesomeIcons.phoneVolume),
                  onTap: () => launch("tel://422506705"),
                  subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start,children: <Widget>[
                    Text("(42) 250 6705",style: TextStyle(
                        fontFamily: "SenRegular"
                    ),),
                  ],),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                ListTile(
                    title: Text("Horarios",style: TextStyle(
                        fontFamily: "SenBold"
                    ),),
                    leading: FaIcon(FontAwesomeIcons.solidClock),
                    subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start,children: <Widget>[
                      Text('Lunes:         08:00 - 19:30',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                      Text('Martes:       08:00 - 19:30',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                      Text('Miércoles:  08:00 - 19:30',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                      Text('Jueves:       08:00 - 19:30',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                      Text('Viernes:      08:00 - 19:30',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                      Text('Sábado:      08:00 - 13:00',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                      Text('Domingo:    CERRADO',style: TextStyle(
                          fontFamily: "SenRegular"
                      ),),
                    ],)),*/
                Container(height: 20,),


              ])
      ),
    );
  }
}