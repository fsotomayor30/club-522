
import 'package:club522_app/Modulos/MenuComida/Menu_screen.dart';
import 'package:club522_app/Modulos/Opciones/Opciones_screen.dart';
import 'package:club522_app/Modulos/Promocion/Promocion_view.dart';
import 'package:club522_app/Modulos/Ubicacion/Ubicacion_view.dart';
import 'package:club522_app/Modulos/VIP/VIP_screen.dart';
import 'package:flutter/material.dart';
class MenusPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MenuList();
  }
}

class MenuList extends State<MenusPage>  {
  int _currentIndex = 0;

  List<BottomNavigationBarItem> containers_menu = [
    BottomNavigationBarItem(
      icon: Icon(Icons.local_dining),
        backgroundColor: Colors.black,
        title: Text("Menus", style: TextStyle(fontFamily: "SenRegular"),)
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.volume_down),
        backgroundColor: Colors.black,
        title: Text("Promociones",style: TextStyle(
            fontFamily: "SenRegular"
        ))
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.star),
        backgroundColor: Colors.black,
        title: Text("VIP",style: TextStyle(
            fontFamily: "SenRegular"
        ))
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.map),
        backgroundColor: Colors.black,
        title: Text("Ubicación",style: TextStyle(
            fontFamily: "SenRegular"
        ))
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.menu),
        backgroundColor: Colors.black,
        title: Text("Menu",style: TextStyle(
            fontFamily: "SenRegular"
        ))
    )
  ];

List<Widget> _children= [
  Container(child:MenuComidaPage()),
  Container(child:PromocionPage()),
  Container(child:VIPPage()),
  Container(child:UbicacionScreen()),
  Container(child:MasOpcionesPage()),

];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: (_currentIndex == 4)? Text("Más opciones",style: TextStyle(
            fontFamily: "SenRegular"
        )) : Text(""),
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // this will be set when a new tab is tapped
        items: containers_menu,
      ),
      body: _children[_currentIndex],
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/Imagenes/fondo app.jpg"), fit: BoxFit.cover
              )
          ),
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  child: Image.asset('assets/Imagenes/egipcialogogrande.png'),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(0,0,0,1),
                      image: DecorationImage(
                      image: NetworkImage(
                        'https://firebasestorage.googleapis.com/v0/b/egipciafestival-45700.appspot.com/o/piramides.jpg?alt=media&token=27ff8a9d-bfcb-4de9-acb6-461c62e70e08',
                      ),
                      fit: BoxFit.cover,

                    ),
                  )
                //color: Color.fromRGBO(238,153,61,1),
              ),
              ListTile(
                title: Text(
                  'Facebook',
                  style: TextStyle(fontFamily: 'Avenir', color: Colors.white),
                ),
                leading: CircleAvatar(
                  child: Image.asset('assets/Imagenes/facebook.png'),
                  backgroundColor: Colors.transparent,
                ),
                onTap: () async {
/*                    if (await canLaunch(
                      "https://www.facebook.com/Egipcia-Festival-108757777339739/")) {
                    await launch("https://www.facebook.com/Egipcia-Festival-108757777339739/");
                  }*/
                },
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
              ),
              ListTile(
                title: Text(
                  'Instagram',
                  style: TextStyle(fontFamily: 'Avenir', color: Colors.white),
                ),
                leading: CircleAvatar(
                  child: Image.asset('assets/Imagenes/instagram.png'),
                  backgroundColor: Colors.transparent,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
                onTap: () async {
/*                    if (await canLaunch(
                      "https://www.instagram.com/egipciafestival/")) {
                    await launch(
                        "https://www.instagram.com/egipciafestival/");
                  }*/
                },
              ),

            ],
          ),
        ),
      ),
    );
  }
}
