import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class _ContactCategory extends StatelessWidget {
  const _ContactCategory({Key key, this.icon, this.children}) : super(key: key);

  final IconData icon;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: themeData.dividerColor))),
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.subtitle,
        child: SafeArea(
          top: false,
          bottom: false,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(vertical: 24.0),
                width: 72.0,
                child: Icon(icon, color: Colors.black),
              ),
              Expanded(child: Column(children: children)),
            ],
          ),
        ),
      ),
    );
  }
}

class _ContactItem extends StatelessWidget {
  const _ContactItem(
      {Key key, this.icon, this.lines, this.tooltip, this.onPressed})
      : assert(lines.length > 1),
        super(key: key);

  final FaIcon icon;
  final List<String> lines;
  final String tooltip;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return MergeSemantics(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ...lines
                      .sublist(0, lines.length - 1)
                      .map<Widget>((String line) => Text(line)),
                  Text(lines.last, style: themeData.textTheme.caption),
                ],
              ),
            ),
            (icon != null)
                ? SizedBox(
                    width: 72.0,
                    child: IconButton(
                      icon: icon,
                      color: Colors.black,
                      onPressed: onPressed,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}

class UbicacionScreen extends StatefulWidget {
  @override
  UbicacionState createState() => UbicacionState();
}


class UbicacionState extends State<UbicacionScreen> {

  @override
  Future initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
      body:
      Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Container(
                  height: 250,
                  child: Stack(
                    children: <Widget>[
                      Image.asset("assets/Imagenes/cafeBanner.jpg", width: mediaQueryData.width, height: mediaQueryData.height*0.2,),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: Image.asset("assets/Imagenes/club522.png", width: 150, height: 150)),

                    ],
                  ),
                ),

              ),
              ListTile(
                title: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Text("Club 522 nace en Chillán el año 2015 como un bar de café, cuyo formato de venta"
                      "se distingue por su atención personalizada, siendo el Barista el encargado de generar"
                      "una experiencia de consumo en torno al café.",textAlign: TextAlign.justify, style: TextStyle(
                    fontFamily: "SenRegular"
                  ),),
                ),
              ),
              Container(height: 20,),
              ListTile(
                title: Text("Ubicación", style: TextStyle(
                    fontFamily: "SenBold"
                  ),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  Text('Isabel Riquelme #522',style: TextStyle(
                    fontFamily: "SenRegular"
                ),),
                  Text('Chillán',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                ],),
                leading: Icon(Icons.location_on),
                trailing: Icon(Icons.arrow_forward_ios),
                onTap: () async {
                  if (await canLaunch(
                      "https://www.google.com/maps/place/Club+522/@-36.607222,-72.1016447,17z/data=!3m1!4b1!4m5!3m4!1s0x9668d7d5e60fdf21:0x8b2342473a4580ca!8m2!3d-36.607222!4d-72.099456")) {
                    await launch(
                        "https://www.google.com/maps/place/Club+522/@-36.607222,-72.1016447,17z/data=!3m1!4b1!4m5!3m4!1s0x9668d7d5e60fdf21:0x8b2342473a4580ca!8m2!3d-36.607222!4d-72.099456");
                  }
                }
              ),
              ListTile(
                title: Text("Teléfono",style: TextStyle(
                    fontFamily: "SenBold"
                ),),
                leading: FaIcon(FontAwesomeIcons.phoneVolume),
                onTap: () => launch("tel://422506705"),
                subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start,children: <Widget>[
                  Text("(42) 250 6705",style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                ],),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                title: Text("Horarios",style: TextStyle(
                    fontFamily: "SenBold"
                ),),
                leading: FaIcon(FontAwesomeIcons.solidClock),
                subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start,children: <Widget>[
                  Text('Lunes:         08:00 - 19:30',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                  Text('Martes:       08:00 - 19:30',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                  Text('Miércoles:  08:00 - 19:30',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                  Text('Jueves:       08:00 - 19:30',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                  Text('Viernes:      08:00 - 19:30',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                  Text('Sábado:      08:00 - 13:00',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                  Text('Domingo:    CERRADO',style: TextStyle(
                      fontFamily: "SenRegular"
                  ),),
                ],)),
              Container(height: 20,),


            ])
      ),
      /*CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(

            expandedHeight: _appBarHeight,
            pinned: _appBarBehavior == AppBarBehavior.pinned,
            floating: _appBarBehavior == AppBarBehavior.floating ||
                _appBarBehavior == AppBarBehavior.snapping,
            snap: _appBarBehavior == AppBarBehavior.snapping,
            actions: <Widget>[],
            flexibleSpace: FlexibleSpaceBar(
              title: Container(
                  width: mediaQueryData.width * 0.5,
                  child: AutoSizeText(
                    "CLUB 522",
                    //style: TextStyle(fontSize: 20),
                    maxLines: 1,
                  )),
              background: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Image.asset(
                    "assets/Imagenes/cafeBanner.jpg",
                    color: Color.fromRGBO(255, 255, 255, 0.7),
                    colorBlendMode: BlendMode.modulate,
                    fit: BoxFit.cover,
                    height: _appBarHeight,
                  ),
                  *//*Image.asset(
                      "assets/Imagenes/Locales/cafe 522.jpg",
                      fit: BoxFit.cover,
                      height: _appBarHeight,
                    ),*//*
                  // This gradient ensures that the toolbar icons are distinct
                  // against the background image.
                  const DecoratedBox(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, .35),
                        colors: <Color>[Color(0xC0000000), Color(0x00000000)],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(<Widget>[
              AnnotatedRegion<SystemUiOverlayStyle>(
                value: SystemUiOverlayStyle.dark,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text("Club 522 nace en Chillán el año 2015 como un bar de café, cuyo formato de venta"
                        "se distingue por su atención personalizada, siendo el Barista el encargado de generar"
                        "una experiencia de consumo en torno al café."),
                  ),
                ),
              ),
              _ContactCategory(
                icon: Icons.location_on,
                children: <Widget>[
                  _ContactItem(
                    icon: FaIcon(FontAwesomeIcons.map, color: Colors.black,),
                    tooltip: 'Send personal e-mail',
                    onPressed: () async {
                    if (await canLaunch(
                      "https://www.google.com/maps/place/Club+522/@-36.607222,-72.1016447,17z/data=!3m1!4b1!4m5!3m4!1s0x9668d7d5e60fdf21:0x8b2342473a4580ca!8m2!3d-36.607222!4d-72.099456")) {
                    await launch(
                        "https://www.google.com/maps/place/Club+522/@-36.607222,-72.1016447,17z/data=!3m1!4b1!4m5!3m4!1s0x9668d7d5e60fdf21:0x8b2342473a4580ca!8m2!3d-36.607222!4d-72.099456");
                  }
                    },
                    lines: const <String>[
                      'Isabel Riquelme #522',
                      'Chillán',
                    ],
                  ),
*//*                  _ContactItem(
                    icon: FaIcon(FontAwesomeIcons.map),
                    tooltip: 'Send work e-mail',
                    onPressed: () {
                      _scaffoldKey.currentState.showSnackBar(const SnackBar(
                        content: Text(
                            'Summon your favorite e-mail application here.'),
                      ));
                    },
                    lines: const <String>[
                      'aliconnors@example.com',
                      'Work',
                    ],
                  ),*//*
                ],
              ),
              _ContactCategory(
                icon: FontAwesomeIcons.phone,
                children: <Widget>[
                  _ContactItem(
                    icon: FaIcon(FontAwesomeIcons.phoneVolume, color: Colors.black,),
                    tooltip: 'Open map',
                    onPressed: () => launch("tel://422506705"),
                    lines: const <String>[
                      '(42) 250 6705',
                      'Teléfono',
                    ],
                  ),
                  *//*_ContactItem(
                    icon: FaIcon(FontAwesomeIcons.map),
                    tooltip: 'Open map',
                    onPressed: () {
                      _scaffoldKey.currentState.showSnackBar(const SnackBar(
                        content:
                            Text('This would show a map of Mountain View.'),
                      ));
                    },
                    lines: const <String>[
                      '1600 Amphitheater Parkway',
                      'Mountain View, CA',
                      'Work',
                    ],
                  ),
                  _ContactItem(
                    icon: FaIcon(FontAwesomeIcons.map),
                    tooltip: 'Open map',
                    onPressed: () {
                      _scaffoldKey.currentState.showSnackBar(const SnackBar(
                        content: Text(
                            'This would also show a map, if this was not a demo.'),
                      ));
                    },
                    lines: const <String>[
                      '126 Severyns Ave',
                      'Mountain View, CA',
                      'Jet Travel',
                    ],
                  ),*//*
                ],
              ),
              _ContactCategory(
                icon: FontAwesomeIcons.solidClock,
                children: <Widget>[
                  _ContactItem(
                    icon: FaIcon(FontAwesomeIcons.clock, color: Colors.black,),
                    tooltip: 'Open map',
                    onPressed: () {
                      _scaffoldKey.currentState.showSnackBar(const SnackBar(
                        content:
                            Text('This would show a map of San Francisco.'),
                      ));
                    },
                    lines: const <String>[
                      'Lunes:         08:00 - 19:30',
                      'Martes:       08:00 - 19:30',
                      'Miércoles:  08:00 - 19:30',
                      'Jueves:       08:00 - 19:30',
                      'Viernes:      08:00 - 19:30',
                      'Sábado:      08:00 - 13:00',
                      'Domingo:    CERRADO',
                      'Horario de atención',
                    ],
                  ),
                ],
              )
*//*              _ContactCategory(
                icon: Icons.today,
                children: <Widget>[
                  _ContactItem(
                    lines: const <String>[
                      'Birthday',
                      'January 9th, 1989',
                    ],
                  ),
                  _ContactItem(
                    lines: const <String>[
                      'Wedding anniversary',
                      'June 21st, 2014',
                    ],
                  ),
                  _ContactItem(
                    lines: const <String>[
                      'First day in office',
                      'January 20th, 2015',
                    ],
                  ),
                  _ContactItem(
                    lines: const <String>[
                      'Last day in office',
                      'August 9th, 2018',
                    ],
                  ),
                ],
              ),*//*
            ]),
          ),
        ],
      ),*/
    );
  }
}
