import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class MenuComidaPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Container(
            height: mediaQueryData.height,
            width: mediaQueryData.width,
            color: Colors.white,
            child: ListView(
              children: <Widget>[
                ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Producto 1", style: TextStyle(fontSize: 12, fontFamily: "SenBold")),
                          Text("\$1.500CLP", style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold, fontFamily: "SenRegular"),),
                        ],
                      ),
                      Image.asset("assets/Imagenes/producto1.jpeg", width: mediaQueryData.width*0.4),
                    ],
                  ),
                    onTap: (){
                      Navigator.of(context).pushNamed('/MenuItem');
                    },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Producto 1", style: TextStyle(fontSize: 12,fontFamily: "SenBold")),
                          Text("\$1.500CLP", style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold,fontFamily: "SenRegular"),),
                        ],
                      ),
                      Image.asset("assets/Imagenes/producto2.jpeg", width: mediaQueryData.width*0.4),
                    ],
                  ),
                  onTap: (){
                    Navigator.of(context).pushNamed('/MenuItem');
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Producto 1", style: TextStyle(fontSize: 12,fontFamily: "SenBold")),
                          Text("\$1.500CLP", style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold,fontFamily: "SenRegular"),),
                        ],
                      ),
                      Image.asset("assets/Imagenes/producto3.jpeg", width: mediaQueryData.width*0.4),
                    ],
                  ),
                  onTap: (){
                    Navigator.of(context).pushNamed('/MenuItem');
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Producto 1", style: TextStyle(fontSize: 12,fontFamily: "SenBold")),
                          Text("\$1.500CLP", style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold,fontFamily: "SenRegular"),),
                        ],
                      ),
                      Image.asset("assets/Imagenes/producto1.jpeg", width: mediaQueryData.width*0.4),
                    ],
                  ),
                  onTap: (){
                    Navigator.of(context).pushNamed('/MenuItem');
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Producto 1", style: TextStyle(fontSize: 12,fontFamily: "SenBold")),
                          Text("\$1.500CLP", style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold,fontFamily: "SenRegular"),),
                        ],
                      ),
                      Image.asset("assets/Imagenes/producto2.jpeg", width: mediaQueryData.width*0.4),
                    ],
                  ),
                  onTap: (){
                    Navigator.of(context).pushNamed('/MenuItem');
                  },
                ),
              ],
            )
        ),
      ),
    );
  }
}