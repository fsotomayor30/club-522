import 'package:flutter/material.dart';


class MenuItemPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: Container(
          width: mediaQueryData.width,
          height: 70,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(0.0),
                          side: BorderSide(color: Colors.white)),
                      color: Colors.black,
                      textColor: Colors.white,
                      padding: EdgeInsets.all(8.0),
                      onPressed: () {
                        /*Navigator.of(context)
                            .pushNamed('/Felicitaciones');*/
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Text(
                              "Agregar a la compra",
                              style: TextStyle(
                                fontFamily: "SenBold",
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          )),
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset("assets/Imagenes/producto2.jpeg", width: mediaQueryData.width),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Text("Titulo Producto 1", textAlign: TextAlign.left,style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "SenBold"),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Descripcion Producto 1", style: TextStyle(fontFamily: "SenRegular"),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("\$1.500CLP", style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold, fontFamily: "SenRegular"),),
                    ),
                  ],
                ),
              ),
            ],
          )
      ),
    );
  }
}