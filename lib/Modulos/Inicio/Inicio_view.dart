import 'package:flutter/material.dart';


class InicioPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.black,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Image.asset("assets/Imagenes/club522.png", width: mediaQueryData.width*0.7,),

              Column(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: mediaQueryData.width*0.6,
                    child: new RaisedButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(0.0),
                          side: BorderSide(color: Colors.white)),
                      padding: const EdgeInsets.all(8.0),
                      textColor: Colors.white,
                      color: Colors.transparent,
                      onPressed: (){

                      },
                      child: new Text("Iniciar Sesión", style: TextStyle(
                          fontFamily: "SenRegular"
                      )),
                    ),
                  ),
                  ButtonTheme(
                    minWidth: mediaQueryData.width*0.6,
                    child: new RaisedButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(0.0),
                          side: BorderSide(color: Colors.transparent)),
                      padding: const EdgeInsets.all(8.0),
                      textColor: Colors.white,
                      color: Colors.transparent,
                      onPressed: (){
                        Navigator.of(context).popAndPushNamed('/Menu');
                      },
                      child: new Text("Iniciar Sesión más tarde",style: TextStyle(
                          fontFamily: "SenRegular"
                      )),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}