import 'package:flutter/material.dart';


class PromocionPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Column(
                  children: <Widget>[
                    Image.asset("assets/Imagenes/promocion.jpeg", width: mediaQueryData.width*0.4),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Promocion 1", style: TextStyle(fontFamily: "SenRegular"),),
                    ),
                    Text("Descripcion Promocion1",style: TextStyle(fontFamily: "SenRegular"),),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: ButtonTheme(
                        minWidth: mediaQueryData.width*0.6,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                              side: BorderSide(color: Colors.black)),
                          padding: const EdgeInsets.all(8.0),
                          textColor: Colors.black,
                          color: Colors.white,
                          onPressed: (){
                            Navigator.of(context).pushNamed('/PromocionItem');
                          },
                          child: new Text("MÁS DETALLES", style: TextStyle(fontFamily: "SenRegular"),),
                        ),
                      ),
                    ),

                  ],
                ),
                onTap: (){

                },
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Column(
                  children: <Widget>[
                    Image.asset("assets/Imagenes/promocion.jpeg", width: mediaQueryData.width*0.4),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Promocion 1", style: TextStyle(fontFamily: "SenRegular"),),
                    ),
                    Text("Descripcion Promocion1",style: TextStyle(fontFamily: "SenRegular"),),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: ButtonTheme(
                        minWidth: mediaQueryData.width*0.6,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                              side: BorderSide(color: Colors.black)),
                          padding: const EdgeInsets.all(8.0),
                          textColor: Colors.black,
                          color: Colors.white,
                          onPressed: (){
                            Navigator.of(context).pushNamed('/PromocionItem');
                          },
                          child: new Text("MÁS DETALLES", style: TextStyle(fontFamily: "SenRegular"),),
                        ),
                      ),
                    ),

                  ],
                ),
                onTap: (){

                },
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Column(
                  children: <Widget>[
                    Image.asset("assets/Imagenes/promocion.jpeg", width: mediaQueryData.width*0.4),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Promocion 1", style: TextStyle(fontFamily: "SenRegular"),),
                    ),
                    Text("Descripcion Promocion1",style: TextStyle(fontFamily: "SenRegular"),),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: ButtonTheme(
                        minWidth: mediaQueryData.width*0.6,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                              side: BorderSide(color: Colors.black)),
                          padding: const EdgeInsets.all(8.0),
                          textColor: Colors.black,
                          color: Colors.white,
                          onPressed: (){
                            Navigator.of(context).pushNamed('/PromocionItem');
                          },
                          child: new Text("MÁS DETALLES", style: TextStyle(fontFamily: "SenRegular"),),
                        ),
                      ),
                    ),

                  ],
                ),
                onTap: (){

                },
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Column(
                  children: <Widget>[
                    Image.asset("assets/Imagenes/promocion.jpeg", width: mediaQueryData.width*0.4),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Promocion 1", style: TextStyle(fontFamily: "SenRegular"),),
                    ),
                    Text("Descripcion Promocion1",style: TextStyle(fontFamily: "SenRegular"),),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: ButtonTheme(
                        minWidth: mediaQueryData.width*0.6,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                              side: BorderSide(color: Colors.black)),
                          padding: const EdgeInsets.all(8.0),
                          textColor: Colors.black,
                          color: Colors.white,
                          onPressed: (){
                            Navigator.of(context).pushNamed('/PromocionItem');
                          },
                          child: new Text("MÁS DETALLES", style: TextStyle(fontFamily: "SenRegular"),),
                        ),
                      ),
                    ),

                  ],
                ),
                onTap: (){

                },
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Column(
                  children: <Widget>[
                    Image.asset("assets/Imagenes/promocion.jpeg", width: mediaQueryData.width*0.4),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Promocion 1", style: TextStyle(fontFamily: "SenRegular"),),
                    ),
                    Text("Descripcion Promocion1",style: TextStyle(fontFamily: "SenRegular"),),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: ButtonTheme(
                        minWidth: mediaQueryData.width*0.6,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                              side: BorderSide(color: Colors.black)),
                          padding: const EdgeInsets.all(8.0),
                          textColor: Colors.black,
                          color: Colors.white,
                          onPressed: (){
                            Navigator.of(context).pushNamed('/PromocionItem');
                          },
                          child: new Text("MÁS DETALLES", style: TextStyle(fontFamily: "SenRegular"),),
                        ),
                      ),
                    ),

                  ],
                ),
                onTap: (){

                },
              ),
            ],
          )
        ),
      ),
    );
  }
}