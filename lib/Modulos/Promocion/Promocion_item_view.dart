import 'package:flutter/material.dart';


class PromocionItemPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(child: Image.asset("assets/Imagenes/promocion.jpeg", width: mediaQueryData.width*0.6)),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Text("Titulo Promocion 1", textAlign: TextAlign.left,),
                    ),
                    new SizedBox(
                      height: 10.0,
                      child: new Center(
                        child: new Container(
                          margin: new EdgeInsetsDirectional.only(start: 1.0, end: mediaQueryData.width*0.5),
                          height: 5.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Descripcion Promocion1"),
                    ),


                  ],
                ),
              ),


            ],
          )
      ),
    );
  }
}