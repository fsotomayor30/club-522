import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class MasOpcionesPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          height: mediaQueryData.height,
          width: mediaQueryData.width,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text("Ordenes", style: TextStyle(
                  color: Colors.black54,
                    fontFamily: "SenRegular"
                ),),
                leading: Icon(FontAwesomeIcons.clipboardList, color: Colors.black,),
              ),
              ListTile(
                  title: Text("Configuración", style: TextStyle(
                    color: Colors.black54,
                      fontFamily: "SenRegular"
                  )),
                  leading: Icon(FontAwesomeIcons.cog, color: Colors.black,),
              ),
              ListTile(
                  title: Text("Politica de privacidad", style: TextStyle(
                    color: Colors.black54,
                      fontFamily: "SenRegular"
                  ),),
                  leading: Icon(FontAwesomeIcons.solidQuestionCircle, color: Colors.black,),
              ),
              ListTile(
                  title: Text("Términos y condiciones", style: TextStyle(
                    color: Colors.black54,
                      fontFamily: "SenRegular"
                  ),),
                leading: Icon(FontAwesomeIcons.listAlt, color: Colors.black,),
              ),
              ListTile(
                  title: Text("Soporte Técnico", style: TextStyle(
                    color: Colors.black54,
                      fontFamily: "SenRegular"
                  ),),
                  leading: Icon(FontAwesomeIcons.mobileAlt, color: Colors.black,),
              ),
            ],
          )
      ),
    );
  }
}