import 'package:club522_app/routes.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import 'Modulos/Inicio/Inicio_view.dart';


Future main() async {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}
class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return new SplashScreen(
        seconds: 4,
        navigateAfterSeconds: new MyAppAfterSplash(),
        image: new Image.asset("assets/Imagenes/club522.png",width: mediaQueryData.width*0.7),
        backgroundColor: Colors.black,
        styleTextUnderTheLoader: new TextStyle(),
        loaderColor: Colors.transparent,
        loadingText: Text("El café como a ti te gusta", style: TextStyle(color: Colors.white, fontSize: 20,
            fontFamily: "SenRegular"
        ), textAlign: TextAlign.center,),
        photoSize: 100.0,
    );
  }
}

class MyAppAfterSplash extends StatelessWidget {
  final appTitle = 'Cafe 522';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: InicioPage(),
      routes: buildAppRoutes(),
    );

  }
}
